module CodeAno
    def find_project_folder(git_folder : Path | Nil) : Path
        if git_folder == Nil
            git_folder = Path[ENV.fetch("GIT_DIR", "./.git")].expand(home: true)
        end

        if File.folder? git_folder
            next_dir = git_folder.dirname
            
            return false if next_dir == dir
            return find_project_folder(next_dir)
        else
            return git_folder
        end
    end
end