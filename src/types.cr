require "digest/sha1"

module CodeAno
    enum ObjectType
        Blob
        Tree
        Pack
    end


    class Object
        getter typ, content

        def initialize(@typ : ObjectType, @content : Bytes)
        end

        def self.fromFile(filepath : Path | String, typ : ObjectType)
            # Creates an object from a filepath

            # Open File & get contents
            file = File.open(filepath, encoding: "utf-8")
            file_bytes = Bytes.new file.size
            file.read_fully file_bytes
            self.new typ, file_bytes
            
        end

        def self.fromHash(hash : Bytes)
            # Retrieve an object from a hash

            # Find directories in .git/objects starting with the first 2 bytes of the hash
            # Find the object in that directory by the final 38 bytes
            file = File.open(filepath)
            # Decompress using zlib
            header_bytes = Array(UInt8).new 0
            file_iterator = file.each_byte
            file_iterator.each do |current|
                    header_bytes << current
            end

            header = String.new b.to_unsafe
            typ_str = header[0,4].downcase.titleize
            typ = CodeAno::ObjectType.parse type_str
            size = header[5,header.size-1].to_i64
            file_iterator.seek header.size+1
            content = file_iterator.read_bytes(size)
            
            self.new(typ, content)
            # Retrieve the object type from first 4 bytes
            # Read from space to first \u000 byte to get content length
            # Retrieve content 
        end

        def header
            "#{self.typ.to_s.downcase} #{self.size.to_s}\u0000"
        end

        def size
            self.content.size
        end

        def str_content
            String.new self.content
        end

        def content_with_header
            "#{self.header}#{self.str_content}"
        end


        def hash
            Digest::SHA1.hexdigest self.content_with_header
        end
    end

    record FileFlags, type : File::Type, sets : File::Flags, perms : File::Permissions
    record TreeObject, mode : FileFlags, type : CodeAno::ObjectType, hash : String, name : String

    class Tree < Object
        property objects : Array(TreeObject)

        def initialize(@content : Bytes)
            @typ = CodeAno::ObjectType::Tree
            @objects = Array(TreeObject).new
            self.parseContent(@content)
        end

        def self.fromFile(filepath : Path | String)
            super(filepath, CodeAno::ObjectType::Tree)
        end

        def parseContent()
            # TODO: read in @content and extract TreeObjects into @objects
        end

        # TODO: addToTree where mode is an integer
        # NOTE: https://stackoverflow.com/questions/737673/how-to-read-the-mode-field-of-git-ls-trees-output

        def addToTree(mode : FileFlags, typ : ObjectType, hash : String, name : String)
            self.addToTree CodeAno::TreeObject.new mode, typ, hash, name
        end

        def addToTree(object : TreeObject)
            existing_object = self.objects.find { |iobject| iobject.name == object.name }
            if existing_object
                self.objects.delete(existing_object)
            end
            self.objects << TreeObject
        end
    end

    class Blob < Object
    end

    class Pack
    end
end